
User.create( name:'STI-UFF', email: 'sti@gmail.com', password: '123456' )
                 
fusca = Product.create(name:'Fusca', price: 3.50, quantity: 7)
sorvete = Product.create(name:'Sorvete', price: 4.30, quantity: 10)
sushi = Product.create(name:'Sushi', price: 6.50, quantity: 2)


fusca.photo = Rails.root.join("db/seed_images/01.jpg").open
sorvete.photo = Rails.root.join("db/seed_images/02.jpg").open
sushi.photo = Rails.root.join("db/seed_images/03.jpg").open

fusca.save!
sorvete.save!
sushi.save!