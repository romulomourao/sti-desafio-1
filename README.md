# Desafio 1: Padrão de Projetos e Orientação a Objetos

__OBS:__ Para fazer o sistema me inspirei num site que a Rozana me mostrou. Esse site é uma referencia em design, merece ser visto:
[http://yihatabeads.storenvy.com/](http://yihatabeads.storenvy.com/)
  
## Considerações

1. Como havia comentado antes de iniciar o projeto, fiquei na dúvida a respeito do. nível de complexidade exigida, principalmente na questão do Pagamento. Pensei em várias abordagens, porém acabei optando por um `enum` na tabela **Order**, não porque é a melhor forma, mas porque me economizou tempo e resolveu de certa forma. Claro que se fosse algo mais elaborado que precisasse representar as diferentes particularidades de cada meio de pagamento, essa forma não seria a melhor.

2. Vocês provavelmente vão ver alguns pedaços de código em lugares que não deveriam estar, mas na maioria das vezes foi pra economizar tempo e facilitar a resolução do problema. Creio que poderia ter feito melhor em alguns casos, como criação de **Decorator**, por exemplo, em vez de helper. No entanto estou ciente dessas "falhas". 

3. Uma das questões era simular o comportamento _desfazer_ antes de completar a ação. Vocês sugeriram pesquisar o **Command Pattern**, eu dei uma olhada e una 2 artigos, mas confesso que não tive tempo para me aprofundar e aplicar no projeto. Por isso, acabei simulando com javascript, atrasando a requisição ajax para que ela não seja executada imediatamente após clicar no botão. O javascript também não está nem perto das boas práticas, mas tenho consciência disso...rsrs

4. Apesar de não ter sido requisito, eu aproveitei a oportunidade para usar o **Redis** e o **Docker**. O Redis usei pra salvar o carrinho do usuário, já que é uma informação que não precisa ser persistida no banco e o Docker foi mais para adequar ao padrão das aplicações novas e me aprofundar na criação do dockerfile e docker-compose, já que vamos precisar migrar muitas aplicações em operações. 

## Instruções para rodar o projeto

> É preciso ter o docker e docker-compose para funcionar

1. Rode o comando `docker-compose build` para buildar a imagem
2. Antes de subir o container da aplicação, rode os seguintes comandos: 

    - `docker-compose run web rails db:create`
    - `docker-compose run web rails db:migrate`
    - `docker-compose run web rails db:seed`

3. Após isso, rode `docker-compose up` para subir os containers 
4. Acesse `localhost:3000`
5. Com aplicação aberta, faça o login com as seguintes credenciais: 

    - Usuário: __sti@gmail.com__
    - Senha: __123456__

6. Agora basta utilizar o sistema! 
