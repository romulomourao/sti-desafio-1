Rails.application.routes.draw do
  devise_for :users
  get 'carts/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  resources :products

  resource :cart, only: [:show] do
    put 'add/:product_id', to: 'carts#add', as: :add_to
    put 'remove/:product_id', to: 'carts#remove', as: :remove_from
    post 'checkout', to: 'carts#checkout'
  end

  resources :orders, only: [:index]
end
