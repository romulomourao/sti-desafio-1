class Product < ApplicationRecord
  
  mount_uploader :photo, PhotoUploader

  validates :price, :name, :photo, presence: true

  def cart_action(current_user_id)
    if $redis.sismember "cart#{current_user_id}", id
      "Remove from"
    else
      "Add to"
    end
  end
end
