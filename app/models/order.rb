class Order < ApplicationRecord
  belongs_to :user
  has_many :order_items
  
  enum payment: {boleto: 0, credito: 1}
end
