class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def show
    @product = Product.find(params[:id])
  end


  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = 'Produto cadastrado com sucesso.'
      return redirect_to root_path
    end
    render :new
  end

  private

  def product_params
    params.require(:product).permit(:name, :price, :photo, :quantity)
  end
  
end