module ApplicationHelper

  def esgotado?(produto)
    produto.quantity.zero?
  end

  def esta_no_carrinho?(produto)
    $redis.sismember "cart#{current_user.id}", produto.id
  end
end
