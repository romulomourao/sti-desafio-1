module CheckoutService
  class << self
    def create(params, user)
      payment = params[:payment].to_i
      products = Product.find(params[:cart])
      order = Order.create(user: user, payment: payment)
      products.each do |product|
        OrderItem.create(order: order, product: product, price: product.price)
        product.decrement!(:quantity, 1)
      end
    end
  end
end