var Cart = {};

$(window).on('load', function () {
  $('a[data-target]').click(function (e) {
    e.preventDefault();
    $this = $(this).closest('a');
    url = $this.data('target');
    if (url.match(/\/remove/)) {
      $this.closest('tr').hide();
    }
    ajax(url, 'put');
  });


  $('#checkout').click(function (e) {
    e.preventDefault();
    showModal();
    $this = $(this).closest('a');
    url = $this.data('url');
    let payment = `&payment=${$("input[name='payment']:checked").val()}`;
    url += payment
    checkout(url);
  });

  $('#undo-checkout').click(function (e) {
    console.log('cancelando');
    e.preventDefault();
    undo_checkout(Cart.checkout);
  });

});


function ajax(url, tipo) {
  console.log("AJAX 1");
  $.ajax({
    url: url,
    type: tipo,
    success: function (data) {
      location.reload();
    }
  });
}


function checkout(url){
  Cart.checkout = setTimeout(() => {
    ajax(url, 'post');
    console.log('COMPRA FINALIZADA!');
    $('#checkout-processing').hide(500);
  }, 3000);
}
function undo_checkout(checkout){
  clearTimeout(checkout);
  $('#checkout-processing').hide(500);
}

function showModal(){
  const modal = $('#checkout-processing');
  modal.show(500);
  modal.css('display', 'flex');
}