FROM ruby:2.4.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /app-sti
WORKDIR /app-sti

COPY Gemfile /app-sti/Gemfile
COPY Gemfile.lock /app-sti/Gemfile.lock

RUN bundle install

COPY . /app-sti